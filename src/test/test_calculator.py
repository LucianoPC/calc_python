import unittest
from calculator import Calculator


class CalculatorTests(unittest.TestCase):

    def test_sum(self):
        calculator = Calculator()
        sum_total = calculator.sum(5, 5)

        self.assertEqual(10, sum_total)

    def test_subtraction(self):
        calculator = Calculator()
        number_one = 5
        number_two = 3
        result = 2

        self.assertEqual(result, calculator.sub(number_one, number_two))

    def test_division_positive_numbers(self):
        calculator = Calculator()
        self.assertEqual(calculator.division(10, 2), 5)

    def test_division_negative_numbers(self):
        calculator = Calculator()
        self.assertEqual(calculator.division(-10, -2), 5)

    def test_division_positive_and_negative_numbers(self):
        calculator = Calculator()
        self.assertEqual(calculator.division(-10, 2), -5)

    def test_division_by_zero(self):
        calculator = Calculator()
        self.assertRaises(ZeroDivisionError, calculator.division, 5, 0)

    def test_multiply_is_equal(self):
        calculator = Calculator()
        number_one = 4
        number_two = 5
        result = 20

        self.assertEqual(result,
                         calculator.calc_multiply(number_one, number_two))

    def test_multiply_is_not_equal(self):
        calculator = Calculator()

        number_one = 4
        number_two = 5
        result = 22

        self.assertNotEquals(result,
                             calculator.calc_multiply(number_one, number_two))
