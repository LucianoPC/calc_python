from setuptools import setup, find_packages

setup(
    name='calculator',
    description="Python Calculator Library",
    version='0.1',
    url='https://gitlab.com/LucianoPC/calc_python',
    author='Luciano Prestes Cavalcanti',
    author_email='lucianopcbr@gmail.com ',
    license='LICENSE.md',
    packages=find_packages(),
    test_suite='nose.collector',
    )
